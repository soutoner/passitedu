/**
 * Created by adrian on 7/8/15.
 */
(function($) {
    // The rel attribute is the userID you would want to follow
    // Uses a div with id = notify-users to notify the users

    var classes = {
        default:    'btn-info',
        following:  'btn-success',
        unfollow:   'btn-danger'
    };

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function() {
        $('button.followButton').on('click', function (e) {
            e.preventDefault();
            $button = $(this);
            if ($button.hasClass(classes.following)) {
                // Unfollow Action
                $.ajax({
                    type:       'POST',
                    url:        '/unsubscribe',
                    dataType:   'JSON',
                    data:       'id=' + $button.attr('rel')
                }).done(function(data, textStatus, jqXHR) {
                    notify_success(jqXHR.responseJSON.success_message);
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    handleAjaxError(jqXHR, textStatus, errorThrown);
                });

                $button.removeClass(classes.following);
                $button.removeClass(classes.unfollow);
                $button.addClass(classes.default);
                $button.text('Suscribirse');
            } else {
                // Follow action
                $.ajax({
                    type:       'POST',
                    url:        '/subscribe',
                    dataType:   'JSON',
                    data:       'id=' + $button.attr('rel')
                }).done(function(data, textStatus, jqXHR) {
                    $button.removeClass(classes.default);
                    $button.addClass(classes.following);
                    $button.text('Suscrito');
                    notify_success(jqXHR.responseJSON.success_message);
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    handleAjaxError(jqXHR, textStatus, errorThrown);
                });
            }
        });

        $('button.followButton').hover(function () {
            $button = $(this);
            if ($button.hasClass(classes.following)) {
                $button.addClass(classes.unfollow);
                $button.text('Cancelar');
            }
        }, function () {
            if ($button.hasClass(classes.following)) {
                $button.removeClass(classes.unfollow);
                $button.text('Suscrito');
            }
        });

        // Handle AJAX ERROR
        //----------------------------------------------
        function handleAjaxError(jqXHR, textStatus, errorThrown) {
            // The user is not logged in
            if(jqXHR.status == 401){
                return window.location.href = '/login';
            } else {
                $('#notify-user').remove();
                var notifyErrorDiv =
                    "<div class='alert alert-danger alert-dismissible' role='alert' id='notify-user'>"
                    +"<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"
                    +'Ups, algo ha ido mal.'
                    +"</div>"
                $('body > .container-fluid').prepend(notifySuccessDiv);
            }
        }

        // Handle success message and notify the user
        //----------------------------------------------
        function notify_success(success_message) {
            $('#notify-user').remove();
            var notifySuccessDiv =
                "<div class='alert alert-success alert-dismissible' role='alert' id='notify-user'>"
                    +"<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"
                    +success_message
                +"</div>"
            $('body > .container-fluid').prepend(notifySuccessDiv);
        }
    });

})(jQuery);