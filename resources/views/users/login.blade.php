@extends('../layouts.master')

@section('title', 'Iniciar Sesión')

@section('content')
    <div style="margin-top: 20px" class="col-md-6 col-md-offset-3">
        <div class="panel panel-info" >
            <!-- panel header !-->
            <div class="panel-heading">
                <div class="panel-title">Iniciar Sesión</div>
            </div>
            <!-- panel body !-->
            <div style="padding-top:20px" class="panel-body" >
                <form method="post" action="login" class="form-horizontal">
                    {!! csrf_field() !!}
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input type="text" name="username" value="{{ old('username') }}" placeholder="Email/Nombre de usuario" class="form-control" />
                    </div>
                    <div style="margin: 20px 0 20px 0;" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input type="password" name="password" class="form-control" placeholder="Contraseña"/>
                    </div>
                    <input type="checkbox" name="remember"> Recuérdame
                    <div style="border-top: 1px solid#888; padding-top: 20px;margin-top: 10px;" class="form-group">
                        <div style="float:right; font-size: 80%; margin-right:10px;">
                            <a href="#">¿Has olvidado la contraseña?</a>
                        </div>
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary">Login</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- end panel body !-->
        </div>
    </div>
@endsection