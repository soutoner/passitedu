{{-- $user -> user that is logged in --}}
{{--TODO: terminar el edit--}}
@extends('../layouts.master')

@section('title', 'Edita tu perfil')

@section('content')
    <div class="col-md-3">
        <img src="{{ $user->photo }}" height="250" width="250">

        <ul class="nav nav-pills nav-stacked" style="width: 89%;margin-top: 20px;" role="tablist">
            <li role="presentation" class="active"><a href="#personal" aria-controls="personal" role="tab" data-toggle="tab">Datos personales</a></li>
            <li role="presentation"><a href="#web" aria-controls="web" role="tab" data-toggle="tab">Web</a></li>
            <li role="presentation"><a href="#delete" aria-controls="delete" role="tab" data-toggle="tab">Eliminar tu cuenta</a></li>
        </ul>
    </div>

    <div class="col-md-8">
        <h2>Edita tu perfil</h2>
            <!-- Tab panes -->
            <div class="tab-content panel panel-default">
                <div role="tabpanel" class="tab-pane active panel-body" id="personal">
                    <form method="POST" action="update">
                        {!! csrf_field() !!}
                        <h4>Datos personales</h4>
                        <div class="form-group">
                            <h5>Nombre</h5>
                            <input type="text" name="name" placeholder="Nombre" value="{{ $user->name }}" maxlength="20" class="form-control">
                        </div>
                        <h5>Descripción</h5>
                        <div class="form-group form-inline">
                            <textarea name="short_desc" class="form-control">{{ $user->short_desc }}</textarea>
                        </div>
                        <button type="submit" class="btn btn-default">Enviar</button>
                    </form>
                </div>

                <div role="tabpanel" class="tab-pane panel-body" id="web">
                    <form method="POST" action="update">
                        {!! csrf_field() !!}
                        <h4>Web</h4>
                        <div class="form-group form-inline">
                            <input type="email" name="email" placeholder="Email" value="{{ $user->email }}" class="form-control" disabled>
                            <input type="text" name="username" id="username-input" placeholder="Nombre de usuario" value="{{ $user->username }}" maxlength="15" class="form-control" disabled>
                            @unless($user->username_changed)
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true" id="change-username" data-toggle="tooltip" data-placement="right"
                                  title="Cambiar el nombre de usuario. Solo podrás cambiarlo una vez, tendra que estar formado por carácteres alfanuméricos y guiones. Debe ser único. Para cambiarlo pulsa en este icono."></span>
                            <script>
                                // Script for enabling-disabling username field
                                $( document ).ready(function() {
                                    $( '#change-username' ).click(function( event ) {
                                        if( $('#username-input').is(':disabled') && '{{ $user->username_changed }}' != '1')
                                            $('#username-input').prop('disabled', false);
                                        else
                                            $('#username-input').prop('disabled', true);
                                    });
                                });
                            </script>
                            @endunless
                        </div>
                        <h5>Contraseña</h5>
                        <div class="form-group">
                            <input type="password" name="old_password" placeholder="Contraseña antigua" class="form-control">
                            <input type="password" name="password" placeholder="Contraseña" class="form-control">
                            <input type="password" name="password_confirmation" placeholder="Confirme la contraseña" class="form-control">
                            <p class="help-block">La contraseña debe tener 8 caracteres, al menos una mayúscula y un numero.</p>
                        </div>
                        <button type="submit" class="btn btn-default">Enviar</button>
                    </form>
                </div>


                <div role="tabpanel" class="tab-pane panel-body" id="delete">
                    <h4>Elimina tu cuenta</h4>
                    <p>Se eliminará tu perfil y todos los datos asociados a ti que haya en nuestra página web (excepto transacciones por motivos de seguridad).</p>
                    <p>Aviso: este proceso es definitivo, una vez realizado no se puede recuperar nada.</p>

                    <form method="POST" action="destroy">
                        {!! csrf_field() !!}
                        <button class="btn btn-danger" onclick="return confirm('¿Estas seguro?')">Eliminar cuenta</button>
                    </form>
                </div>
            </div>
    </div>
@endsection