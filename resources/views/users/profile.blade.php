{{-- $profile -> Usuario del perfil --}}
{{--TODO: terminar el profile--}}
@extends('../layouts.master')

@section('title', $profile->username)

@section('content')
    <div class="col-md-3">
        <img src="{{ $profile->photo }}" height="250" width="250">
    @unless($profile->isLoggedIn())
        <div style="padding-top: 10px;">
            @if(Auth::check() && $profile->followers->contains(Auth::user()->id))
                <button class="btn btn-success followButton" rel="{{ $profile->id }}">Suscrito</button>
            @else
                <button class="btn btn-info followButton" rel="{{ $profile->id }}">Suscribirse</button>
            @endif
        </div>
    @endunless
        <h3>followers</h3>
        <ul>
            @forelse($profile->followers as $usr)
                <li><a href="{{ $usr->username }}">{{ $usr->username }}</a></li>
            @empty
                <li>El usuario no esta following.</li>
            @endforelse
        </ul>

        <h3>following a</h3>
        <ul>
            @forelse($profile->following as $usr)
                <li><a href="{{ $usr->username }}">{{ $usr->username }}</a></li>
            @empty
                <li>El usuario no tiene followers.</li>
            @endforelse
        </ul>
    </div>
    <div class="col-md-8">
        <h1>{{ $profile->name }}<small>{{ ' - @'.$profile->username }}</small></h1>
        <p>{{ $profile->short_desc }}</p>
    </div>
@endsection