@extends('../layouts.master')

@section('title', 'Bienvenido')

@section('content')
    <form method="POST" action="/store">
        {!! csrf_field() !!}
        <div class="form-group">
            <label for="reg-username" class="sr-only">Nombre de usuario</label>
            <input type="text" name="username" class="form-control" id="reg-username" value="{{ old('username') }}" placeholder="Nombre de usuario">
        </div>
        <div class="form-group">
            <label for="reg-email" class="sr-only">Email</label>
            <input type="email" name="email" class="form-control" id="reg-email" value="{{ old('email') }}" placeholder="Email">
        </div>
        <div class="form-group">
            <label for="reg-password" class="sr-only">Contraseña</label>
            <input type="password" name="password" class="form-control" id="reg-password" placeholder="Contraseña">
        </div>
        <div class="form-group">
            <label for="reg-password-confirm" class="sr-only">Confirme la contraseña</label>
            <input type="password" name="password_confirmation" class="form-control" id="reg-password-confirm" placeholder="Confirme la contraseña">
        </div>
        <p class="helper-block">Usa al menos una mayúscula, un número y ocho carácteres</p>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="terms"> Acepto los <a href="#">términos</a>
            </label>
        </div>
        <button type="submit" class="btn btn-default">Enviar</button>
    </form>
@endsection