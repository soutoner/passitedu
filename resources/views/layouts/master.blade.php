<!DOCTYPE html>
<html>
<head lang="es">
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Bootstrap and general css !-->
    <link rel="stylesheet" type="text/css" href="/css/app.css">
    <!-- JQuery and bootstrap js !-->
    <script type="text/javascript" src="/js/app.js"></script>

    @section('head')
        @show

    <title>@yield('title') # PassItEDU</title>
</head>
<body>
    @include('layouts._header')

    <div class="container-fluid">
        @include('layouts/_flash_messages')

        @yield('content')
    </div>

    @include('layouts._footer')
</body>
</html>