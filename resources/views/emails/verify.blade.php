{{--TODO: Personalizar el correo--}}

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Confirma tu dirección de correo</h2>

<div>
    Gracias por crearte una cuenta en PassItEDU.
    Por favor sigue el enlace que aparece más abajo para poder verificar tu dirección de correo
    {{ URL::to('/registrar/verificar/' . $user->confirmation_code) }}.<br/>
</div>

</body>
</html>