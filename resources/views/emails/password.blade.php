<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Aquí tienes tu contraseña provisional</h2>

<div>
    Gracias por crearte una cuenta en PassItEDU.
    Aunque siempre podras acceder mediante el login social (por ejemplo 'conectar con Facebook'),
    te hemos proporcionado una contraseña provisional para que puedas acceder a la web mediante el login:

    <h2>{{ $password }}</h2>

    Te aconsejamos que en los próximos días cambies la contraseña por una más segura y que puedas recordar fácilmente.
    Si quieres hacerlo ahora puedes hacerlo en este enlace.
    {{--TODO: Enlace a cambiar la contraseña--}}<br/>
</div>

</body>
</html>