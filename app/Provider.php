<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    /**
     * RELATIONSHIPS
     */

    public function profiles()
    {
        return $this->hasMany('App\Profile');
    }
}
