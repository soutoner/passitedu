<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Session;
use URL;

class Authenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->guest()) {
            Session::put('previous_url', URL::previous()); // Guardamos la url de donde venimos
            if ($request->ajax()) {
                return response('No estas autorizado.', 401);
            } else {
                return redirect()->route('login')->with('warning_message', 'Debes iniciar sesión para acceder.');
            }
        }

        return $next($request);
    }
}
