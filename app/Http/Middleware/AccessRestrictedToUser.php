<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\User;

class AccessRestrictedToUser
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Find the user profiel by the first segment of the path
        $profile = User::where('username', explode('/', $request->path())[0])->firstOrFail();

        if($this->auth->guest() || ($this->auth->user()->id != $profile->id)){
            return redirect()->route('home')->with('warning_message', 'No tienes permiso para acceder aquí.');
        }

        return $next($request);
    }
}
