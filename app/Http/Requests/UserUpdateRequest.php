<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\User;
use Auth;

class UserUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check() && (Auth::user()->id == User::where('username', explode('/', $this->path())[0])->firstOrFail()->id);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          =>  ['between:2,50', 'alpha_spaces'],
            'short_desc'    =>  ['max:140'],
            'username'      =>  ['between:3,15', 'unique:users', 'alpha_dash'],
            'password'      =>  ['min:8', 'confirmed', 'regex:/^(?=.*[A-Z])(?=.*[0-9]).{6,}$/'],
            'old_password'  =>  ['required_with:password', 'be_old_password']
        ];
    }
}
