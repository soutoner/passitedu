<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use Socialite;
use App\User;
use App\Profile;
use App\Provider;
use App\Jobs\SendPasswordEmail;

class AuthController extends Controller
{
    public function login()
    {
        return view('users.login');
    }

    public function authenticate(Request $request)
    {
        $usernameOrEmail = $request->input('username');
        $keyUsernameOrEmail = (strpos($usernameOrEmail, '@')) ? 'email' : 'username';
        $remember = $request->input('remember');
        $credentials = [
            $keyUsernameOrEmail => $usernameOrEmail,
            'password'          => $request->input('password')
        ];
        $previous_url = Session::get('previous_url','/');

        if (Auth::attempt($credentials, $remember)) {
            return redirect()->intended($previous_url)->with('success_message', 'Bienvenido '.Auth::user()->username.'!');
        } else {
            return back()->withInput()->with('warning_message', 'Combinación de username/email y contraseña incorrecta.');
        }
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('home')->with('success_message', 'Esperamos verte de vuelta pronto!');
    }

    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return Response
     */
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return Response
     */
    public function handleFacebookCallback()
    {
        // users data on facebook
        $facebook = Socialite::driver('facebook')->user();

        // find profile to see if user is registered
        $profile = Profile::whereUid($facebook->getId())->first();

        // User doesn't exits, let's register it
        if (empty($profile)){
            // unwanted accents
            $unwanted_array = array(    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
                'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
                'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
                'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
                'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );

            // First remove possible dots
            $username = str_replace(".","",$facebook->getNickname());
            // Facebook username can be empty so we must create one in that case firstname000 (random string)
            if(empty($username))
                $username = explode(' ', $facebook->getName())[0] . str_random(3);
            // We need to provide a provisional password and we send it to the user
            $password = str_random(5);

            // Remember to remove dots because FB username can have them
            $user = User::create([
                'name'      =>  $facebook->getName(),
                'email'     =>  $facebook->getEmail(),
                'photo'     =>  'https://graph.facebook.com/'.$facebook->getId().'/picture?width=500&height=500',
                'username'  =>  strtr( $username, $unwanted_array ),
                'password'  =>  $password
            ]);

            // Send mail with provisional password
            $this->dispatch(new SendPasswordEmail($user, $password));

            $profile = new Profile();
            $profile->uid = $facebook->getId();
            $profile->provider_id = Provider::where('name', 'facebook')->firstOrFail()->id;
            $profile = $user->profiles()->save($profile);
        }

        // User exists, lets login him
        $profile->access_token = $facebook->token;
        $profile->save();

        $user = $profile->user;

        Auth::login($user);

        return redirect()->route('home')->with('success_message', 'Bienvenido a PassItEDU!');
    }

}
