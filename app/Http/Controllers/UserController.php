<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Event;
use App\Events\UserWasRegistered;
use Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(UserStoreRequest $request)
    {

        $user = User::create($request->only(
            'username',
            'email',
            'password'
        ));

        // save the confirmation code
        $confirmation_code = str_random(30);
        $user->confirmation_code = $confirmation_code;
        $user->save();

        // fire the event for mail confirmation sending
        Event::fire(new UserWasRegistered($user));

        return redirect()->route('home')->with('success_message', 'Bienvenido! Por favor, sigue las instrucciones que hemos enviado a tu correo.');
    }

    /**
     * Verifies the confirmation code of an user
     *
     * @param $confirmation_code
     * @return \Illuminate\Http\RedirectResponse
     */
    public function verify($confirmation_code)
    {
        $user = User::where('confirmation_code', $confirmation_code)->firstOrFail();

        $user->confirmed = 1;
        $user->confirmation_code = null;
        $user->save();

        return redirect()->action('AuthController@login')->with('success_message', 'Has confirmado tu cuenta correctamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $username
     * @return Response
     */
    public function show($username)
    {
        return view('users.profile')->with('profile', User::where('username', $username)->firstOrFail());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $username
     * @return Response
     */
    public function edit($username)
    {
        return view('users.edit')->with('user', Auth::user());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  string  $username
     * @return Response
     */
    public function update(UserUpdateRequest $request, $username)
    {
        $profile = Auth::user();
        $profile->update($request->only(
            'name',
            'short_desc'
        ));

        $password = $request->input('password');
        $username = $request->input('username');

        if(!empty($password))
            $profile->update(['password' => Hash::make($password)]);

        if(!empty($username) && !$profile->username_changed) {
            $profile->update(['username' => $username, 'username_changed' => '1']);
        }

        return redirect()->route('user-edit', [$profile->username])->with('success_message', 'Perfil actualizado con éxito.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $username
     * @return Response
     */
    public function destroy($username)
    {
        User::where('username', $username)->firstOrFail()->delete();

        return redirect()->route('home')->with('success_message', 'Sentimos tu marcha, esperamos verte de vuelta pronto!');
    }

    /**
     * AJAX request to subscribe to an user
     *
     * POST param 'id' -> profile to which the authenticated user is subscribing
     */
    public function subscribe(Request $request)
    {
        try{
            $profile = User::findOrFail($request->input('id'));
            $profile->followers()->attach(Auth::user()->id);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error_message' => 'Ups, el usuario al que intentas suscribirte no existe.']);
        }
        return response()->json(['success_message' => 'Ahora estas suscrito a '.$profile->username]);
    }
    /**
     * AJAX request to unsubscribe to an user
     *
     * POST param 'id' -> profile to which the authenticated user is subscribing
     */
    public function unsubscribe(Request $request)
    {
        try{
            $profile = User::findOrFail($request->input('id'));
            $profile->followers()->detach(Auth::user()->id);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error_message' => 'Ups, el usuario no existe.']);
        }
        return response()->json(['success_message' => 'Ya no estas suscrito a '.$profile->username]);
    }
}
