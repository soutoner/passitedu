<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => 'home', function () {
    if(Auth::check())
        return view('users.home')->with('user', Auth::user());
    else
        return view('welcome');
}]);

// Authentication routes...
Route::get('/login', [
    'as'            => 'login',
    'middleware'    => 'guest',
    'uses'          => 'AuthController@login'
]);
Route::post('/login', 'AuthController@authenticate');
Route::get('/salir', [
    'middleware' => 'auth',
    'uses' => 'AuthController@logout'
]);
Route::get('/auth/facebook', 'AuthController@redirectToFacebook');
Route::get('/auth/facebook/callback', 'AuthController@handleFacebookCallback');

// Registration routes...
Route::get('/registrar', 'UserController@create');
Route::post('/store', 'UserController@store');
Route::get('/registrar/verificar/{confirmationCode}', 'UserController@verify');

// Follow route
Route::group(['middleware' => 'auth'], function () {
    Route::post('/subscribe', 'UserController@subscribe');
    Route::post('/unsubscribe', 'UserController@unsubscribe');
});

// Profile routes

Route::group(['prefix' => '/{username}'], function () {
    Route::get('/', [
        'as'    =>  'profile',
        'uses'  =>  'UserController@show'
    ]);
    Route::get('/editar', [
        'as'            =>  'user-edit',
        'middleware'    =>  'logged.in',
        'uses'          =>  'UserController@edit'
    ]);
    Route::post('/update', 'UserController@update');
    Route::post('/destroy', 'UserController@destroy');
});
