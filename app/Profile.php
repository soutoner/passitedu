<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = ['uid', 'access_token', 'access_token_secret'];

    /**
     * RELATIONSHIPS
     */

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function provider()
    {
        return $this->belongsTo('App\Provider');
    }
}
