<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;
use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Validator for string with letters, spaces and tildes
        Validator::extend('alpha_spaces', function($attribute, $value)
        {
            return preg_match('/^([a-zA-ZÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöùúûüýøþÿÐdŒ-\s])+$/i', $value);
        });

        // Validator for password, must match old password
        Validator::extend('be_old_password', function($attribute, $value)
        {
            if(!Auth::check()) return false;

            if(Auth::attempt(['email' => Auth::user()->email, 'password' => $value]))
                return true;
            else
                return false;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() == 'local') {
            $this->app->register('Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider');
            $this->app->register('Laracasts\Generators\GeneratorsServiceProvider');
            $this->app->register('Barryvdh\Debugbar\ServiceProvider');
        }
    }
}
