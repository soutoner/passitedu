<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Auth;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['username', 'email', 'password', 'name', 'photo', 'short_desc'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];


    /**
     * RELATIONSHIPS
     */
    public function followers()
    {
        return $this->belongsToMany('App\User', 'user_follows', 'user_id', 'follower_id');
    }

    public function following()
    {
        return $this->belongsToMany('App\User', 'user_follows', 'follower_id', 'user_id');
    }

    public function profiles()
    {
        return $this->hasMany('App\Profile');
    }

    /**
     * GETTERS
     */
    //TODO: remove placehold image
    public function getPhotoAttribute($value){
        return (empty($value)) ? 'http://placekitten.com/g/200/300' : $value;
    }

    /**
     * AUXILIARY FUNCTIONS
     */

    // Comprueba si el usuario de perfil esta logueado
    public function isLoggedIn(){
        return Auth::check() && ($this->id == Auth::user()->id);
    }
}
