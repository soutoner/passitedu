<?php
/**
 * Created by PhpStorm.
 * User: adrian
 * Date: 7/20/15
 * Time: 2:55 PM
 */

class FollowUserTest extends TestCase{

    protected static $profile, $auth;

    public function setUp()
    {
        parent::setUp();

        self::$profile = factory(App\User::class)->create();
        self::$auth = factory(App\User::class)->create();
    }

    public function testButtonAppears()
    {
        $this->visit('/'.self::$profile->username)
            ->see('Suscribirse');
    }

    public function testButtonOnOwnProfileDisappears()
    {
        $this->actingAs(self::$profile)
            ->visit('/'.self::$profile->username)
            ->dontSee('Suscribirse');
    }

    public function testSubscribeNotLoggedIn302()
    {
        Session::start();

        $this->post('/subscribe', ['_token' => csrf_token(), 'id' => self::$profile->id])
            ->assertRedirectedTo(route('login'));
    }

    public function testUnsubscribeNotLoggedIn302()
    {
        Session::start();

        $this->post('/unsubscribe', ['_token' => csrf_token(), 'id' => self::$profile->id])
            ->assertRedirectedTo(route('login'));
    }

    public function testSubscribeCorrect()
    {
        Session::start();

        $this->actingAs(self::$auth)
            ->post('/subscribe', ['_token' => csrf_token(), 'id' => self::$profile->id])
            ->seeJsonContains(['success_message' => 'Ahora estas suscrito a '.self::$profile->username])
            ->assertTrue(self::$profile->followers->contains(self::$auth->id));
    }

    public function testUnsubscribeCorrect()
    {
        Session::start();

        $this->actingAs(self::$auth)
            ->post('/subscribe', ['_token' => csrf_token(), 'id' => self::$profile->id])
            ->seeJsonContains(['success_message' => 'Ahora estas suscrito a '.self::$profile->username])
            ->post('/unsubscribe', ['_token' => csrf_token(), 'id' => self::$profile->id])
            ->seeJsonContains(['success_message' => 'Ya no estas suscrito a '.self::$profile->username])
            ->assertFalse(self::$profile->followers->contains(self::$auth->id));
    }


    public function tearDown()
    {
        App\User::destroy([self::$profile->id, self::$auth->id]);
    }

}