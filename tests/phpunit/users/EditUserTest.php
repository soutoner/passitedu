<?php

/**
 * Created by PhpStorm.
 * User: adrian
 * Date: 7/22/15
 * Time: 12:56 PM
 */

class EditUserTest extends TestCase
{
    protected static $profile, $auth;

    public function setUp()
    {
        parent::setUp();

        self::$profile = factory(App\User::class)->create(['password' => 'profile1P']);
        self::$auth = factory(App\User::class)->create();
    }

    public function testEditRoute()
    {
        $this->actingAs(self::$profile)
            ->visit(route('user-edit', [self::$profile->username]))
            ->seePageIs(route('user-edit', [self::$profile->username]))
            ->see('Editar');
    }

    public function testEditRouteImpossibleIfNotLoggedIn()
    {
        $this->visit(route('user-edit', [self::$profile->username]))
            ->seePageIs(route('home'))
            ->see('No tienes permiso para acceder aquí.');
    }

    public function testEditRouteImpossibleIfNotYours()
    {
        $this->actingAs(self::$auth)
            ->visit(route('user-edit', [self::$profile->username]))
            ->seePageIs(route('home'))
            ->see('No tienes permiso para acceder aquí.');
    }

    public function testChangeName()
    {
        $before = App\User::where('email', self::$profile->email)->firstOrFail();

        $this->actingAs(self::$profile)
            ->visit(route('user-edit', [self::$profile->username]))
            ->type('My Name Is pepito', 'name')
            ->press('Enviar')
            ->see('Perfil actualizado con éxito.')
            ->assertEquals('My Name Is Pepito', self::$profile->name);
        // data is persisted
        $this->seeInDatabase('users', ['name' => 'My Name Is Pepito']);
        // the rest remains the same
        $this->assertNotEquals($before->name, self::$profile->name);
        $this->assertEquals(
            $before->lists('email','username', 'short_desc', 'password', 'photo', 'changed_username', 'confirmed', 'confirmation'),
            self::$profile->lists('email','username', 'short_desc', 'password', 'photo', 'changed_username', 'confirmed', 'confirmation')
        );
    }

    public function testChangeShortDesc()
    {
        $before = App\User::where('email', self::$profile->email)->firstOrFail();

        $this->actingAs(self::$profile)
            ->visit(route('user-edit', [self::$profile->username]))
            ->type('Pues asi soy yo loco', 'short_desc')
            ->press('Enviar')
            ->see('Perfil actualizado con éxito.')
            ->assertEquals('Pues asi soy yo loco', self::$profile->short_desc);
        // data is persisted
        $this->seeInDatabase('users', ['short_desc' => 'Pues asi soy yo loco']);
        // the rest remains the same
        $this->assertNotEquals($before->short_desc, self::$profile->short_desc);
        $this->assertEquals(
            $before->lists('name', 'email','username', 'password', 'photo', 'changed_username', 'confirmed', 'confirmation'),
            self::$profile->lists('name', 'email','username', 'password', 'photo', 'changed_username', 'confirmed', 'confirmation')
        );
    }

    public function testChangePassword()
    {
        $this->startSession();

        $before = App\User::where('email', self::$profile->email)->firstOrFail();

        $this->actingAs(self::$profile)
            ->post('/'.self::$profile->username.'/update', [
                '_token'                 =>  csrf_token(),
                'old_password'           =>  'profile1P',
                'password'               =>  'password1P',
                'password_confirmation'  =>  'password1P'
            ])
            ->assertRedirectedTo('/'.self::$profile->username.'/editar');
        // password is saved
        $this->assertTrue(Auth::attempt([
                'email'     =>  self::$profile->email,
                'password'  =>  'password1P'
            ]));
        // the rest remains the same
        $this->assertEquals(
            $before->lists('name', 'email','username', 'short_desc', 'photo', 'changed_username', 'confirmed', 'confirmation'),
            self::$profile->lists('name', 'email','username', 'short_desc', 'photo', 'changed_username', 'confirmed', 'confirmation')
        );
    }

    public function testChangePasswordBadOld()
    {
//        TODO: pass test and more test
        $before = App\User::where('email', self::$profile->email)->firstOrFail();

        $this->actingAs(self::$profile)
            ->withSession(['user' => self::$profile])
            ->post('/'.self::$profile->username.'/update', [
                '_token'                 =>  csrf_token(),
                'old_password'           =>  'asd',
                'password'               =>  'password1P',
                'password_confirmation'  =>  'password1P'
            ])
            ->see('hola k ase')
            ->assertRedirectedTo('/'.self::$profile->username.'/editar');


        // password is saved
        $this->assertFalse(Auth::attempt([
            'email'     =>  self::$profile->email,
            'password'  =>  'password1P'
        ]));
        // the rest remains the same
        $this->assertEquals(
            $before->lists('name', 'email','username', 'short_desc', 'photo', 'changed_username', 'confirmed', 'confirmation'),
            self::$profile->lists('name', 'email','username', 'short_desc', 'photo', 'changed_username', 'confirmed', 'confirmation')
        );

    }


    public function tearDown()
    {
        App\User::destroy([self::$profile->id, self::$auth->id]);
    }
}