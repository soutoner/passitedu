<?php
/**
 * Created by PhpStorm.
 * User: adrian
 * Date: 7/6/15
 * Time: 4:48 PM
 */

use Illuminate\Foundation\Testing\DatabaseTransactions;

class CreateUserTest extends TestCase
{
    use DatabaseTransactions;

    private $success_message = 'Bienvenido! Por favor, sigue las instrucciones que hemos enviado a tu correo.';

    public function setUp()
    {
        parent::setUp();

        $this->visit('/registrar')
            ->type('example@amena.com', 'email')
            ->type('FooBaRCiTo', 'username')
            ->type('password1P', 'password')
            ->type('password1P', 'password_confirmation');

    }

    public function testRegistrarRoute()
    {
        $this->visit('/')
            ->click('Regístrate')
            ->seePageIs('/registrar');
    }

    public function testRegistrarCorrecto()
    {
        $this->expectsEvents(App\Events\UserWasRegistered::class);

        $this->check('terms')
            ->press('Enviar')
            ->seePageIs('/')
            ->see($this->success_message)
            ->seeInDatabase('users', ['email' => 'example@amena.com']);
    }

    public function testUsernameRequerido()
    {
        $this->type('', 'username')
            ->check('terms')
            ->press('Enviar')
            ->dontSee($this->success_message)
            ->notSeeInDatabase('users', ['email' => 'example@amena.com']);
    }

    public function testUsernameInvalidoArroba()
    {
        $this->type('hola@', 'username')
            ->check('terms')
            ->press('Enviar')
            ->dontSee($this->success_message)
            ->notSeeInDatabase('users', ['email' => 'example@amena.com']);
    }

    public function testUsernameInvalidoPunto()
    {
        $this->type('hola.kase', 'username')
            ->check('terms')
            ->press('Enviar')
            ->dontSee($this->success_message)
            ->notSeeInDatabase('users', ['email' => 'example@amena.com']);
    }

    public function testContrasenaRequerido()
    {
        $this->type('', 'password')
            ->check('terms')
            ->press('Enviar')
            ->dontSee($this->success_message)
            ->notSeeInDatabase('users', ['email' => 'example@amena.com']);
    }

    public function testContrasenaConfirmRequerido()
    {
        $this->type('', 'password')
            ->check('terms')
            ->press('Enviar')
            ->dontSee($this->success_message)
            ->notSeeInDatabase('users', ['email' => 'example@amena.com']);
    }

    public function testContrasenaNoSegura()
    {
        $this->type('asd', 'password')
            ->type('asd', 'password_confirmation')
            ->check('terms')
            ->press('Enviar')
            ->dontSee($this->success_message)
            ->notSeeInDatabase('users', ['email' => 'example@amena.com']);
    }

    public function testContrasenaNoCoinciden()
    {
        $this->type('asd', 'password')
            ->check('terms')
            ->press('Enviar')
            ->dontSee($this->success_message)
            ->notSeeInDatabase('users', ['email' => 'example@amena.com']);
    }

    public function testNoAceptaTerminos()
    {
        $this->press('Enviar')
            ->dontSee($this->success_message)
            ->notSeeInDatabase('users', ['email' => 'example@amena.com']);
    }

    public function testPasswordIsHashed()
    {
        $this->check('terms')
            ->press('Enviar')
            ->assertNotEquals('password1P', App\User::where('email', 'example@amena.com')->first()->password);
    }

    public function testUsernameIsLowercased()
    {
        $this->check('terms')
            ->press('Enviar')
            ->assertEquals('foobarcito', App\User::where('email', 'example@amena.com')->first()->username);
    }

    public function testConfirmEmail()
    {
        $this->check('terms')
            ->press('Enviar')
            ->assertEquals('foobarcito', App\User::where('email', 'example@amena.com')->first()->username);

        $this->assertEquals(App\User::where('email', 'example@amena.com')->first()->confirmed, '0');

        $this->assertNotEmpty(App\User::where('email', 'example@amena.com')->first()->confirmation_code);

        $this->visit('/registrar/verificar/'.App\User::where('email', 'example@amena.com')->first()->confirmation_code)
            ->assertEquals(App\User::where('email', 'example@amena.com')->first()->confirmed, '1');

        $this->assertEmpty(App\User::where('email', 'example@amena.com')->first()->confirmation_code);

    }

}