<?php
/**
 * Created by PhpStorm.
 * User: adrian
 * Date: 7/20/15
 * Time: 1:09 PM
 */

class AuthUserTest extends TestCase {

    protected static $auth;

    public function setUp()
    {
        parent::setUp();

        self::$auth = factory(App\User::class)->create(['password' => 'password1P']);
    }

    public function testLoginRouteHeader()
    {
        $this->visit('/')
            ->click('Iniciar Sesión')
            ->see('Iniciar Sesión')
            ->seePageIs(route('login'));
    }

    public function testLoginRouteImpossibleIfLoggedin()
    {
        $this->actingAs(self::$auth)
            ->visit(route('login'))
            ->seePageIs('/');
    }

    public function testLoginUsernameCorrecto()
    {
        $this->visit(route('login'))
            ->type(self::$auth->username, 'username')
            ->type('password1P', 'password')
            ->press('Login')
            ->seePageIs('/')
            ->see('Bienvenido '.self::$auth->username.'!')
            ->see('Hola '.self::$auth->username)
            ->assertTrue(Auth::check());
    }

    public function testLoginEmailCorrecto()
    {
        $this->visit(route('login'))
            ->type(self::$auth->email, 'username')
            ->type('password1P', 'password')
            ->press('Login')
            ->seePageIs('/')
            ->see('Bienvenido '.self::$auth->username.'!')
            ->see('Hola '.self::$auth->username)
            ->assertTrue(Auth::check());
    }

    public function testLoginUsernameIncorrecto()
    {
        $this->visit(route('login'))
            ->type('hola', 'username')
            ->type('password1P', 'password')
            ->press('Login')
            ->seePageIs(route('login'))
            ->see('Combinaci&oacute;n de username/email y contrase&ntilde;a incorrecta.');
    }

    public function testLoginEmailIncorrecto()
    {
        $this->visit(route('login'))
            ->type('hola@a.a', 'username')
            ->type('password1P', 'password')
            ->press('Login')
            ->seePageIs(route('login'))
            ->see('Combinaci&oacute;n de username/email y contrase&ntilde;a incorrecta.');
    }

    public function testLoginPasswordIncorrecto()
    {
        $this->visit(route('login'))
            ->type(self::$auth->username, 'username')
            ->type('password2P', 'password')
            ->press('Login')
            ->seePageIs(route('login'))
            ->see('Combinaci&oacute;n de username/email y contrase&ntilde;a incorrecta.');
    }

    public function testHeaderChange()
    {
        $this->actingAs(self::$auth)
            ->visit('/')
            ->see(self::$auth->username);
    }

    public function testLogoutRoute()
    {
        $this->actingAs(self::$auth)
            ->visit('/salir')
            ->assertFalse(Auth::check());
    }

    public function testLogoutRouteImpossibleIfNotLoggedin()
    {
        $this->visit('/salir')
            ->seePageIs(route('login'))
            ->see('Debes iniciar sesi&oacute;n para acceder.');
    }

    public function tearDown()
    {
        App\User::destroy([self::$auth->id]);
    }

}