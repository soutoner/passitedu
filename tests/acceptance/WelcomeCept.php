<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('follow someone');
$I->amGoingTo('click follow button without being logged in');
$I->amOnPage('/');
$I->see('foobar');
$I->click('button.followButton');
$I->wait(1);
$I->seeCurrentUrlEquals('/login');
